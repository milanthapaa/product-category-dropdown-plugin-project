<?php 
/**
 * Adds Product_Category_Toggle widget.
 */
class Category_Toggle_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'productcategory_widget', // Base ID
			esc_html__( 'Product Category Listing', 'pct' ), // Name
			array( 'description' => esc_html__( 'Widget to display Product Category and Sub-category', 'pct' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget']; // Whatever you want to display before widget (<div>, etc)

		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		// Widget Content Output
		
		function my_category_widget() {
			$args = array(
				'taxonomy' => 'product_cat',
				'orderby' => 'title',
				'order' => 'ASC',
				'hide_empty' => false,
				'parent'   => 0
			);
			$product_cat = get_terms( $args );

			foreach ($product_cat as $parent_product_cat)
			{

				echo '<li>
				<a href="'.get_term_link($parent_product_cat->term_id).'">'.$parent_product_cat->name.'</a>';
				$child_args = array(
					'taxonomy' => 'product_cat',
					'hide_empty' => false,
					'parent'   => $parent_product_cat->term_id
				);
				$child_product_cats = get_terms( $child_args );
				if($child_product_cats){
					echo '<ul>';
					foreach ($child_product_cats as $child_product_cat)
					{
						echo '<li><a href="'.get_term_link($child_product_cat->term_id).'">'.$child_product_cat->name.'</a></li>';
					}
					echo '</ul>';
				}
				echo '</li>';
			}
		}
		echo '<nav class="left-nav hidden-xs hidden-sm hidden-md side-menu ">';
		echo '<ul>';
		my_category_widget();
		echo '</ul>';
		echo '</nav>';

		echo $args['after_widget']; // Whatever you want to display before widget (</div>, etc)
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Product Category Title', 'pct' );
		?>

		<!-- TITLE -->
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'pct' ); ?>
		</label> 

		<input class="widefat" 
		id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" 
		name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" 
		type="text" 
		value="<?php echo esc_attr( $title ); ?>">
	</p>

	<?php 
}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

		return $instance;
	}

} // class Product_Category_Toggle



?>