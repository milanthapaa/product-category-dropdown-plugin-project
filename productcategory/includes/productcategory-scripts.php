<?php 

// Add Script
function pct_add_scripts() {
	// Add Main CSS
	wp_enqueue_style('pct-main-style', plugins_url(). '/productcategory/css/style.css');

	// Add Main JS
	wp_enqueue_script('pct-main-script', plugins_url(). '/productcategory/js/main.js', array(), false, true);

	wp_register_script( $handle, $src, $deps = array(), $ver = false, $in_footer = false )

}

add_action('wp_enqueue_scripts', 'pct_add_scripts');

?>