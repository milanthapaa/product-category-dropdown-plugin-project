<?php
/**
 * Plugin Name: Product Category
 * Plugin URI: https://www.linkedin.com/in/thapamilan/
 * Description: Basic WordPress Plugin That Display Toggled Product Category Through Widget
 * Version:     1.0.0
 * Author:      Milan Thapa
 * Author URI:  https://www.linkedin.com/in/thapamilan/
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: pct
 */

// Exit if accessed directly
if(!defined('ABSPATH')) {
	exit;
}

// Load Scripts
require_once(plugin_dir_path(__FILE__). '/includes/productcategory-scripts.php');

// Load Class
require_once(plugin_dir_path(__FILE__). '/includes/productcategory-class.php');


// Register Widget
	function register_productcategory() {
		register_widget('Category_Toggle_Widget');
	}

// Hook in function
	add_action('widgets_init', 'register_productcategory');